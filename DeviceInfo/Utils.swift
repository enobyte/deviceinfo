//
//  Utils.swift
//  DeviceInfo
//
//  Created by OCIN on 15/3/19.
//  Copyright © 2019 OneConnect. All rights reserved.
//

import Foundation
import UIKit

func showExpertAlert(sender: UIViewController, title :String, message:String, callback:@escaping (AnyObject) -> Void) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: {
        response in
        callback(true as AnyObject)
    }))
    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: {
        response in
        callback(false as AnyObject)
    }))
    sender.present(alert, animated: true, completion: nil)
}
