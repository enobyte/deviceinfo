//
//  MainViewController.swift
//  DeviceInfo
//
//  Created by OCIN on 12/3/19.
//  Copyright © 2019 OneConnect. All rights reserved.
//

import UIKit
import CoreBluetooth
import AVFoundation
import CoreLocation


class MainViewController: UIViewController, CLLocationManagerDelegate{
    @IBOutlet weak var scroolview: UIScrollView!
    var managerBluetooth:CBCentralManager!
    var valueBluetooth: String = ""
    var locationManager:CLLocationManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        determineMyCurrentLocation()
        managerBluetooth          = CBCentralManager()
        managerBluetooth.delegate = self as? CBCentralManagerDelegate
        bluetoothComp.text = "Bluetooth Component : \(UIDevice.current.bluetooth(central: managerBluetooth))"
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setUpDeviceInfoView()
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        locationInfo.text = "Location: (\(userLocation.coordinate.latitude), \(userLocation.coordinate.longitude))"
        altitude.text = "Altitude: \(userLocation.altitude)"
        speed.text = "Speed: \(userLocation.speed)"
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("userlocation \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if(status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
    
    func showLocationDisabledPopUp() {
        showExpertAlert(sender: self, title: "Enable Location on “App”?", message: "Allow App to capture your location") { response in
            if response.boolValue{
                if let url = URL(string: UIApplication.openSettingsURLString) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
            }
        }
    }
    
    private func setUpDeviceInfoView(){
        
        //DeviceInfo
        scroolview.addSubview(deviceInfo)
        scroolview.addSubview(discSpace)
        scroolview.addSubview(freediscSpace)
        scroolview.addSubview(screenResolution)
        scroolview.addSubview(screenBrightness)
        scroolview.addSubview(deviceBrandName)
        scroolview.addSubview(deviceType)
        scroolview.addSubview(deviceModel)
        scroolview.addSubview(deviceName)
        scroolview.addSubview(cpuUsage)
        scroolview.addSubview(totalMemory)
        scroolview.addSubview(usedMemory)
        scroolview.addSubview(batreLevel)
        scroolview.addSubview(isCharging)
        scroolview.addSubview(fullyCharging)
        scroolview.addSubview(useHeadPhone)
        scroolview.addSubview(UUID)
        scroolview.addSubview(wifiMac)
        scroolview.addSubview(isSimulator)
        scroolview.addSubview(IMEI)
        scroolview.addSubview(wifiComp)
        scroolview.addSubview(dataComp)
        scroolview.addSubview(GPSComp)
        scroolview.addSubview(phoneComp)
        scroolview.addSubview(bluetoothComp)
        scroolview.addSubview(earphoneComp)
        scroolview.addSubview(nfcComp)
        
        //CarrierInfo
        scroolview.addSubview(carrierInfo)
        scroolview.addSubview(carrierName)
        scroolview.addSubview(carrierVOIP)
        scroolview.addSubview(carrierCountry)
        scroolview.addSubview(carrierISO)
        scroolview.addSubview(carrierMobile)
        scroolview.addSubview(carrierMobileNetwork)

        //Device OS Info
        scroolview.addSubview(deviceOSInfo)
        scroolview.addSubview(systemName)
        scroolview.addSubview(systemDeviceType)
        scroolview.addSubview(systemVersion)
        scroolview.addSubview(language)
        scroolview.addSubview(country)
        scroolview.addSubview(timeZone)
        scroolview.addSubview(jailBroken)
        
        //Network Info
        scroolview.addSubview(networkInfo)
        scroolview.addSubview(ipAddress)
        scroolview.addSubview(netAddress)
        scroolview.addSubview(cellWifi)
        scroolview.addSubview(routerAddress)
        
        //App Information
        scroolview.addSubview(AppInfo)
        scroolview.addSubview(appVersion)
        scroolview.addSubview(appBuildVersion)
        scroolview.addSubview(appName)
        scroolview.addSubview(debugAttach)
        scroolview.addSubview(appStatus)
        scroolview.addSubview(appStatusSecond)
        scroolview.addSubview(loginFailureTime)
        
        //Geografic Info
        scroolview.addSubview(geoInfo)
        scroolview.addSubview(locationInfo)
        scroolview.addSubview(altitude)
        scroolview.addSubview(speed)
        
        //User Information
        scroolview.addSubview(userInfo)
        scroolview.addSubview(contact)
        
        
        scroolview.addConstraintsWithFormat(format: "V:|-10-[v0]-3-[v1]-3-[v2]-3-[v3]-3-[v4]-3-[v5]-3-[v6]-3-[v7]-3-[v8]-3-[v9]-3-[v10]-3-[v11]-3-[v12]-3-[v13]-3-[v14]-3-[v15]-3-[v16]-3-[v17]-3-[v18]-3-[v19]-3-[v20]-3-[v21]-3-[v22]-3-[v23]-3-[v24]-3-[v25]-3-[v26]-10-[v27]-3-[v28]-3-[v29]-3-[v30]-3-[v31]-3-[v32]-3-[v33]-10-[v34]-3-[v35]-3-[v36]-3-[v37]-3-[v38]-3-[v39]-3-[v40]-3-[v41]-10-[v42]-3-[v43]-3-[v44]-3-[v45]-3-[v46]-10-[v47]-3-[v48]-3-[v49]-3-[v50]-3-[v51]-3-[v52]-3-[v53]-3-[v54]-10-[v55]-3-[v56]-3-[v57]-3-[v58]-10-[v59]-3-[v60]-3-|",
                                            views: deviceInfo, discSpace, freediscSpace, screenResolution, screenBrightness,
                                            deviceBrandName, deviceType, deviceModel, deviceName, cpuUsage, totalMemory, usedMemory, batreLevel,
                                            isCharging, fullyCharging, useHeadPhone, UUID, wifiMac, isSimulator, IMEI, wifiComp, dataComp,
                                            GPSComp, phoneComp, bluetoothComp, earphoneComp, nfcComp, carrierInfo, carrierName, carrierVOIP, carrierCountry, carrierISO, carrierMobile, carrierMobileNetwork, deviceOSInfo, systemName, systemDeviceType, systemVersion, language, country, timeZone, jailBroken, networkInfo, ipAddress, netAddress, cellWifi, routerAddress, AppInfo, appVersion, appBuildVersion, appName, debugAttach, appStatus, appStatusSecond, loginFailureTime, geoInfo, locationInfo, altitude, speed, userInfo, contact)
        
        //DeviceInfo
        scroolview.addConstraintsWithFormat(format: "H:|-10-[v0]|", views: deviceInfo)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: discSpace)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: freediscSpace)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: screenResolution)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: screenBrightness)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: deviceBrandName)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: deviceType)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: deviceModel)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: deviceName)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: cpuUsage)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: totalMemory)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: usedMemory)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: batreLevel)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: isCharging)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: fullyCharging)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: useHeadPhone)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-25-|", views: UUID)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: wifiMac)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: isSimulator)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: IMEI)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: wifiComp)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: GPSComp)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: phoneComp)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: bluetoothComp)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: earphoneComp)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: nfcComp)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: dataComp)
        
        //CarrierInfo
        scroolview.addConstraintsWithFormat(format: "H:|-10-[v0]|", views: carrierInfo)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: carrierName)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: carrierVOIP)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: carrierCountry)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: carrierISO)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: carrierMobile)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: carrierMobileNetwork)
        
        //Device OS Info
        scroolview.addConstraintsWithFormat(format: "H:|-10-[v0]|", views: deviceOSInfo)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: systemName)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: systemDeviceType)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: systemVersion)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: language)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: country)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: timeZone)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: jailBroken)
        
        //Network Info
        scroolview.addConstraintsWithFormat(format: "H:|-10-[v0]|", views: networkInfo)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: ipAddress)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: netAddress)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: cellWifi)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: routerAddress)
        
        //Application Info
        scroolview.addConstraintsWithFormat(format: "H:|-10-[v0]|", views: AppInfo)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: appVersion)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: appBuildVersion)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: appName)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: debugAttach)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: appStatus)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: appStatusSecond)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: loginFailureTime)
        
        //Geografic Info
        scroolview.addConstraintsWithFormat(format: "H:|-10-[v0]|", views: geoInfo)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: locationInfo)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: altitude)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: speed)
        
        //user Information
        scroolview.addConstraintsWithFormat(format: "H:|-10-[v0]|", views: userInfo)
        scroolview.addConstraintsWithFormat(format: "H:|-25-[v0]-10-|", views: contact)
        
    }
    
    //DeviceInfo View
    let deviceInfo: UILabel = {
        let device = UILabel()
        device.text = "* Device Hardware Info"
        return device
    }()
    
    let discSpace: UILabel = {
        let disc = UILabel().defaultLabel()
        disc.text = "Disk Space: \(UIDevice.current.totalDiskSpace)"
        return disc
    }()
    
    let freediscSpace: UILabel = {
        let freedisc = UILabel().defaultLabel()
        freedisc.text = "Free Disk Space: \(UIDevice.current.freeDiskSpace)"
        return freedisc
    }()
    
    let screenResolution: UILabel = {
        let screen = UILabel().defaultLabel()
        screen.text = "Screen Resolution: \(UIDevice.current.resolution)"
        return screen
    }()
    
    let screenBrightness: UILabel = {
        let screenBright = UILabel().defaultLabel()
        screenBright.text = "Screen Brightness: \(UIScreen.main.brightness)"
        return screenBright
    }()
    
    let deviceBrandName: UILabel = {
        let deviceBrand = UILabel().defaultLabel()
        deviceBrand.text = "Device Brand Name : \(UIDevice.current.model)"
        return deviceBrand
    }()
    
    let deviceType: UILabel = {
        let devicetype = UILabel().defaultLabel()
        devicetype.text = "Device Type: \(UIDevice.current.model)"
        return devicetype
    }()
    
    let deviceModel: UILabel = {
        let devicemodel = UILabel().defaultLabel()
        devicemodel.text = "Device Model: \(UIDevice.current.modelName)"
        return devicemodel
    }()
    
    let deviceName: UILabel = {
        let devicename = UILabel().defaultLabel()
        devicename.text = "Device Name: \(UIDevice.current.name)"
        return devicename
    }()
    
    let cpuUsage: UILabel = {
        let cpuusage = UILabel().defaultLabel()
        cpuusage.text = "Cpu Usage: \(UIDevice.current.cpuUsage)"
        return cpuusage
    }()
    
    let totalMemory: UILabel = {
        let totalmemory = UILabel().defaultLabel()
        totalmemory.text = "Total Memory: \(String(UIDevice.current.totalMemory)) GB "
        return totalmemory
    }()
    
    let usedMemory: UILabel = {
        let usedMemory = UILabel().defaultLabel()
        usedMemory.text = "Memory Used: \(String(UIDevice.current.memoryUse)) MB"
        return usedMemory
    }()
    
    let batreLevel: UILabel = {
        let batrelevel = UILabel().defaultLabel()
        batrelevel.text = "Battery Level: \(UIDevice.current.batreLvl)"
        return batrelevel
    }()
    
    let isCharging: UILabel = {
        let ischarge = UILabel().defaultLabel()
        ischarge.text = "is Charging: \(UIDevice.current.isCharging)"
        return ischarge
    }()
    
    let fullyCharging: UILabel = {
        let fullycharge = UILabel().defaultLabel()
        fullycharge.text = "Fully Charging: \(UIDevice.current.isFullCharging)"
        return fullycharge
    }()
    
    let useHeadPhone: UILabel = {
        let useheadphone = UILabel().defaultLabel()
        if AVAudioSession.isHeadphonesConnected {
            useheadphone.text = "Use Headphone: Yes"
        }else{
            useheadphone.text = "Use Headphone: No"
        }
        return useheadphone
    }()
    
    let UUID: UILabel = {
        let uuid = UILabel().defaultLabel()
        uuid.text = "Unique Device Identifier: \(UIDevice.current.identifierForVendor!.uuidString)"
        return uuid
    }()
    
    let wifiMac: UILabel = {
        let wifimac = UILabel().defaultLabel()
        wifimac.text = "Wifi Mac: \(UIDevice.current.wifiMacAddr)"
        return wifimac
    }()
    
    let isSimulator: UILabel = {
        let issimulator = UILabel().defaultLabel()
        issimulator.text = "Simulator : \(UIDevice.current.isSimulator)"
        return issimulator
    }()
    
    let IMEI: UILabel = {
        let imei = UILabel().defaultLabel()
        imei.text = "IMEI : No"
        return imei
    }()
    
    let wifiComp: UILabel = {
        let wificomp = UILabel().defaultLabel()
        if !fetchSSIDInfo().isEmpty {
            wificomp.text = "Wifi Component: Yes"
        }else {
            wificomp.text = "Wifi Component: No"
        }
        return wificomp
    }()

    let dataComp: UILabel = {
        let datacomp = UILabel().defaultLabel()
        datacomp.text = "Data Component: \(UIDevice.current.dataComp)"
        return datacomp
    }()
    
    let GPSComp: UILabel = {
        let gpscomp = UILabel().defaultLabel()
        gpscomp.text = "GPS Component: \(UIDevice.current.gpsComp)"
        return gpscomp
    }()
    
    let phoneComp: UILabel = {
        let phonecomp = UILabel().defaultLabel()
        phonecomp.text = "Phone Component: \(UIDevice.current.phoneComp)"
        return phonecomp
    }()
    
    let bluetoothComp: UILabel = {
        let bluetoothcomp = UILabel().defaultLabel()
        bluetoothcomp.text = ""
        return bluetoothcomp
    }()
    
    let earphoneComp: UILabel = {
        let earphonecomp = UILabel().defaultLabel()
        if AVAudioSession.isHeadphonesConnected {
            earphonecomp.text = "Earphone Component: Yes"
        }else{
            earphonecomp.text = "Earphone Component: No"
        }
        return earphonecomp
    }()
    
    let nfcComp: UILabel = {
        let nfccomp = UILabel().defaultLabel()
        if UIDevice.current.isNFCAvailable {
            nfccomp.text = "NFC Component: Yes"
        }else {
            nfccomp.text = "NFC Component: No"
        }
        return nfccomp
    }()
    
    
    //CarrierInfo View
    let carrierInfo: UILabel = {
        let carrierinfo = UILabel()
        carrierinfo.text = "* Carrier Info"
        return carrierinfo
    }()
    
    let carrierName: UILabel = {
        let carriername = UILabel().defaultLabel()
        carriername.text = "Carrier Name: \(UIDevice.current.carrierName)"
        return carriername
    }()
    
    let carrierVOIP: UILabel = {
        let carriervoip = UILabel().defaultLabel()
        if UIDevice.current.allowVoip{
            carriervoip.text = "Carrier Allows VOIP: Yes"
        }else {
            carriervoip.text = "Carrier Allows VOIP: No"
        }
        return carriervoip
    }()
    
    let carrierCountry: UILabel = {
        let carriercountry = UILabel().defaultLabel()
        carriercountry.text = "Carrier Country: \(UIDevice.current.carrierCountry)"
        return carriercountry
    }()
    
    let carrierISO: UILabel = {
        let carrieriso = UILabel().defaultLabel()
        carrieriso.text = "Carrier ISO Country Code: \(UIDevice.current.isoCountry)"
        return carrieriso
    }()
    
    let carrierMobile: UILabel = {
        let carriermobile = UILabel().defaultLabel()
        carriermobile.text = "Carrier Mobile Country Code: \(UIDevice.current.carrierMobileCode)"
        return carriermobile
    }()
    
    let carrierMobileNetwork: UILabel = {
        let carriermobilenetwork = UILabel().defaultLabel()
        carriermobilenetwork.text = "Carrier Mobile Network Code: \(UIDevice.current.carrierMobileNetwork)"
        return carriermobilenetwork
    }()

    /**
        DeviceOSInfo View
    **/
    let deviceOSInfo: UILabel = {
        let deviceosinfo = UILabel()
        deviceosinfo.text = "* Device OS Info"
        return deviceosinfo
    }()
    
    let systemName: UILabel = {
        let systemname = UILabel().defaultLabel()
        systemname.text = "System Name: IOS"
        return systemname
    }()
    
    let systemDeviceType: UILabel = {
        let systemdevicetype = UILabel().defaultLabel()
        systemdevicetype.text = "System Device Type: \(String(describing: ProcessInfo().environment["SIMULATOR_MODEL_IDENTIFIER"]!))"
        return systemdevicetype
    }()
    
    let systemVersion: UILabel = {
        let systemversion = UILabel().defaultLabel()
        systemversion.text = "System Version: \(UIDevice.current.systemVersion)"
        return systemversion
    }()
    
    let language: UILabel = {
        let language = UILabel().defaultLabel()
        language.text = "Language: \(UIDevice.current.language)"
        return language
    }()
    
    let country: UILabel = {
        let country = UILabel().defaultLabel()
        country.text = "Country: \(UIDevice.current.country)"
        return country
    }()
    
    let timeZone: UILabel = {
        let timezone = UILabel().defaultLabel()
        timezone.text = "Time Zone: \(UIDevice.current.timeZone)"
        return timezone
    }()
    
    let jailBroken: UILabel = {
        let jailbroken = UILabel().defaultLabel()
        jailbroken.text = "JailBroken: \(UIDevice.current.hasJailbreak)"
        return jailbroken
    }()
    
    /**
     Network Info View
     **/
    let networkInfo: UILabel = {
        let networkinfo = UILabel()
        networkinfo.text = "* Network Info"
        return networkinfo
    }()
    
    let ipAddress: UILabel = {
        let ipAddress = UILabel().defaultLabel()
        ipAddress.text = "IP Address: \(UIDevice.current.getAddresses().last!.ip)"
        return ipAddress
    }()
    
    let netAddress: UILabel = {
        let netAddress = UILabel().defaultLabel()
        netAddress.text = "Netmask Address: \(UIDevice.current.getAddresses().last!.netmask)"
        return netAddress
    }()
    
    let cellWifi: UILabel = {
        let cellWifi = UILabel().defaultLabel()
        cellWifi.text = "Cellular Or Wifi: \(UIDevice.current.cellOrWifi)"
        return cellWifi
    }()
    
    let routerAddress: UILabel = {
        let routerAddress = UILabel().defaultLabel()
        routerAddress.text = "Router Address: \(UIDevice.current.routerIP)"
        return routerAddress
    }()
    
    /**
     Application Info
     **/
    let AppInfo: UILabel = {
        let appinfo = UILabel()
        appinfo.text = "* App Info"
        return appinfo
    }()
    
    let appVersion: UILabel = {
        let appVersion = UILabel().defaultLabel()
        appVersion.text = "App Version: \(Bundle.main.releaseVersionNumber ?? "")"
        return appVersion
    }()
    
    let appBuildVersion: UILabel = {
        let appBuildVersion = UILabel().defaultLabel()
        appBuildVersion.text = "App Build Version: \(Bundle.main.buildVersionNumber ?? "")"
        return appBuildVersion
    }()
    
    let appName: UILabel = {
        let appName = UILabel().defaultLabel()
        appName.text = "App Name: \(Bundle.main.displayName ?? "")"
        return appName
    }()
    
    let debugAttach: UILabel = {
        let debugAttach = UILabel().defaultLabel()
        debugAttach.text = "Debuger Attach: "
        return debugAttach
    }()
    
    let appStatus: UILabel = {
        let appStatus = UILabel().defaultLabel()
        appStatus.text = "App status: \(UIDevice.current.checkAppInstall)"
        return appStatus
    }()
    
    let appStatusSecond: UILabel = {
        let appStatusSecond = UILabel().defaultLabel()
        appStatusSecond.text = "App status: Start"
        return appStatusSecond
    }()
    
    let loginFailureTime: UILabel = {
        let loginfailuretime = UILabel().defaultLabel()
        loginfailuretime.text = "Login Time:"
        return loginfailuretime
    }()
    
    /**
     Geografic Info
     **/
    let geoInfo: UILabel = {
        let geoInfo = UILabel()
        geoInfo.text = "* Geografic Information"
        return geoInfo
    }()
    
    let locationInfo: UILabel = {
        let locations = UILabel().defaultLabel()
        locations.text = "Location : "
        return locations
    }()
    
    let altitude: UILabel = {
        let altitude = UILabel().defaultLabel()
        altitude.text = "Altitude : "
        return altitude
    }()
    
    let speed: UILabel = {
        let speed = UILabel().defaultLabel()
        speed.text = "Speed : "
        return speed
    }()
    
    //User Information
    let userInfo: UILabel = {
        let userInfo = UILabel()
        userInfo.text = "* User Information"
        return userInfo
    }()
    
    let contact: UILabel = {
        let contact = UILabel().defaultLabel()
        contact.text = "Contact : \(UIDevice.current.getContact)"
        return contact
    }()
    
    
}
